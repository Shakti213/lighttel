defmodule Lighttel.Packet.EndSubscriptionTest do
  use ExUnit.Case

  doctest Lighttel.Packet.EndSubscription
  doctest Lighttel.Packet.EndSubscription.Ack

  alias Lighttel.Packet.EndSubscription

  test "Reject invalid payload" do
    assert_raise UndefinedFunctionError, fn -> EndSubscription.from_payload(<<0, 0>>) end

    assert_raise UndefinedFunctionError, fn ->
      EndSubscription.from_payload(<<0, 0, 0, 6, "hello">>)
    end
  end

  test "Do not add invalid endpoint" do
    assert_raise FunctionClauseError, fn -> EndSubscription.add(%EndSubscription{}, 0) end
    assert_raise FunctionClauseError, fn -> EndSubscription.add(%EndSubscription{}, "") end
  end
end
