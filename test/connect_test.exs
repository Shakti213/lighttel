defmodule Lighttel.Packet.ConnectTest do
  use ExUnit.Case, async: true
  doctest Lighttel.Packet.Connect

  test "Check to_packet" do
    assert Lighttel.Packet.Connect.payload(
             %Lighttel.Packet.Connect{
               compression_support: Lighttel.Compression.algorithms()
             },
             []
           ) == <<1, 512::big-16, 300::big-16, 300::big-16, 1>>

    assert_raise FunctionClauseError, fn -> Lighttel.Packet.Connect.payload(1, []) end
  end
end
