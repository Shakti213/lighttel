defmodule Lighttel.BatchPublishTest do
  use ExUnit.Case, async: true
  doctest Lighttel.Packet.BatchPublish

  alias Lighttel.Packet.BatchPublish

  @bin_single_header <<6 + 14, 256::big-16, 0, 1::big-16, 14, 0, 5, "abcde", 0, 5, "fghij",
                       "hello">>
  @single_header %Lighttel.Packet.Publish{
    message_id: 256,
    endpoint: 1,
    payload: "hello",
    headers: %{
      "abcde" => ["fghij"]
    }
  }

  @bin_no_header <<6, 257::big-16, 0x80, 5::big-16, "hello", 0, "world">>
  @no_header %Lighttel.Packet.Publish{
    message_id: 257,
    endpoint: "hello",
    payload: "world"
  }

  @bin_multiple_headers <<6 + 14 + 14, 257::big-16, 0x80, 5::big-16, "hello", 8 + 5 + 5 + 5 + 5,
                          5::big-16, "abcde", 5::big-16, "fghij", 5::big-16, "abcde", 5::big-16,
                          "klmno", "world">>

  @multiple_headers %Lighttel.Packet.Publish{
    message_id: 257,
    endpoint: "hello",
    payload: "world",
    headers: %{"abcde" => ["fghij", "klmno"]}
  }

  test "Parse empty batch publish" do
    assert BatchPublish.from_payload(<<0>>, []) === %BatchPublish{}
  end

  test "Parse single publish message" do
    assert BatchPublish.from_payload(<<0>> <> @bin_single_header, []) ===
             %BatchPublish{
               messages: [
                 @single_header
               ]
             }
  end

  test "Parse multiple publish messages" do
    header = <<0>>
    payload1 = @bin_single_header
    payload2 = @bin_no_header

    assert BatchPublish.from_payload(header <> payload1 <> payload2, []) ===
             %BatchPublish{
               messages: [
                 @single_header,
                 @no_header
               ]
             }
  end

  test "Parse a compressed message" do
    header = <<0x01>>
    payload1 = @bin_single_header
    payload2 = @bin_multiple_headers
    {_alg, compressed} = Lighttel.Compression.compress(payload1 <> payload2, :deflate, 9)

    assert BatchPublish.from_payload(header <> compressed, []) ===
             %BatchPublish{
               messages: [
                 @single_header,
                 @multiple_headers
               ]
             }
  end

  test "Serialize empty publish batch message" do
    assert BatchPublish.payload(%BatchPublish{}, []) === <<0>>
  end

  test "Serialize single publish batch message" do
    payload = %BatchPublish{
      messages: [
        @multiple_headers
      ]
    }

    assert BatchPublish.payload(payload, []) === <<0>> <> @bin_multiple_headers
  end

  test "Serialize multiple messages" do
    header = <<0>>
    payload1 = @bin_single_header
    payload2 = @bin_multiple_headers
    payload3 = @bin_no_header

    data = %BatchPublish{
      messages: [
        @single_header,
        @multiple_headers,
        @no_header
      ]
    }

    assert BatchPublish.payload(data, []) === header <> payload1 <> payload2 <> payload3
  end

  test "Compress and serialize multiple messages" do
    header = <<1>>
    payload1 = @bin_single_header
    payload2 = @bin_multiple_headers
    payload3 = @bin_no_header

    {_, compressed_payload} =
      Lighttel.Compression.compress(payload1 <> payload2 <> payload3, :deflate, 9)

    data = %BatchPublish{
      messages: [
        @single_header,
        @multiple_headers,
        @no_header
      ]
    }

    assert BatchPublish.payload(data, compression: :deflate, compression_level: 9) ===
             header <> compressed_payload
  end
end
