# Lighttel

This package contains code shared between lighttel servers and clients. For details on the lighttel protocol see [this repository](https://gitlab.com/Shakti213/lighttel-specification).

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `lighttel` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:lighttel, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/lighttel](https://hexdocs.pm/lighttel).

