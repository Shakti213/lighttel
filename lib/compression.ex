defmodule Lighttel.Compression do
  def algorithms do
    [:deflate]
  end

  def algorithm_map() do
    %{:deflate => 0x01}
  end

  def bitfield_to_algorithms(flags) do
    use Bitwise

    algorithm_map()
    |> Enum.map(fn {c, f} -> if((flags &&& f) === f, do: c, else: []) end)
    |> List.flatten()
  end

  def algorithms_to_bitfield(algos) when is_list(algos) do
    supported = algorithm_map()

    algos
    |> Enum.reduce(0, fn x, acc -> acc + Map.fetch!(supported, x) end)
  end

  def single_algo_to_bitfield(:uncompressed) do
    0
  end

  def single_algo_to_bitfield(algo) do
    Map.fetch!(algorithm_map(), algo)
  end

  def bitfield_to_single_algo(field) do
    if field === 0 do
      :uncompressed
    else
      {key, _} =
        algorithm_map()
        |> Enum.find(fn {_x, y} -> y === field end)

      key
    end
  end

  def compress(data, algo, level \\ 9)

  def compress(data, :deflate, level) when level in 0..9 do
    z = :zlib.open()
    :ok = :zlib.deflateInit(z, level)
    compressed = :zlib.deflate(z, data, :finish)
    :ok = :zlib.deflateEnd(z)
    :ok = :zlib.close(z)
    {:deflate, :binary.list_to_bin(compressed)}
  end

  def compress(data, :best, _) do
    original_sz = byte_size(data)
    {:deflate, compressed} = compress(data, :deflate, 9)

    if byte_size(compressed) < original_sz do
      {:deflate, compressed}
    else
      {:uncompressed, data}
    end
  end

  def compress(data, :uncompressed, _) do
    {:uncompressed, data}
  end

  def compress(data, {:best, algos}, _) do
    algos
    |> Enum.reduce({:uncompressed, data}, fn alg, {_, best_data_so_far} = current_best ->
      {_, compressed_data} = compress(data, alg)

      if byte_size(compressed_data) < byte_size(best_data_so_far) do
        {alg, compressed_data}
      else
        current_best
      end
    end)
  end

  def decompress(data, algo, max_size \\ -1)

  def decompress(data, :deflate, max_size) do
    z = :zlib.open()
    :ok = :zlib.inflateInit(z)

    if max_size === nil or max_size <= 0 do
      decompressed = :zlib.inflate(z, data)
      :ok = safe_inflateEnd(z)
      :binary.list_to_bin(decompressed)
    else
      {:ok, data} = safe_inflate(data, z, <<>>, safe_inflator(z, data), max_size)
      data
    end
  end

  def decompress(data, :uncompressed, _) do
    data
  end

  defp safe_inflate(_data, z, total_decompressed, {:finished, decompressed}, _max_size) do
    IO.inspect({:finished, decompressed})
    :ok = safe_inflateEnd(z)
    {:ok, total_decompressed <> :binary.list_to_bin(decompressed)}
  end

  defp safe_inflate(data, z, total_decompressed, {:continue, decompressed}, max_size) do
    IO.inspect(decompressed)
    concat_decompressed = total_decompressed <> :binary.list_to_bin(decompressed)

    if byte_size(concat_decompressed) >= max_size do
      safe_inflateEnd(z)
      {:error, :too_much_data}
    else
      safe_inflate(data, z, concat_decompressed, safe_inflator(z, data), max_size)
    end
  end

  # Use apply in safe inflator to avoid compile warnings related to undefined or private functions
  defp safe_inflator(z, data) do
    otp_ver =
      :erlang.system_info(:otp_release)
      |> List.to_string()
      |> String.to_integer()

    if otp_ver < 20 do
      case apply(:zlib, :inflateChunk, [z, data]) do
        {:mode, decompressed} -> {:continue, decompressed}
        decompressed -> {:finished, decompressed}
      end
    else
      apply(:zlib, :safeInflate, [z, data])
    end
  end

  defp safe_inflateEnd(z) do
    try do
      :zlib.inflateEnd(z)
    rescue
      _ -> :ok
    end

    :zlib.close(z)
  end
end
