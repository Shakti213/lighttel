defmodule Lighttel.String do
  @moduledoc """
  String processing functions
  """

  @doc """
  Converts a lighttel string in binary representation to
  an elixir string.

  Errors are reported via {:error, error}, and ok results
  are reported via {:ok, string_data, rest_of_data}

  ##Examples

      iex>Lighttel.String.from_binary(<<0,2>> <> "ab123")
      {:ok, "ab", "123"}

  """
  def from_binary(<<len::big-16, string_part::binary-size(len), rest::binary>>) do
    if String.valid?(string_part) do
      {:ok, string_part, rest}
    else
      {:error, :invalid_string}
    end
  end

  def from_binary!(bin) do
    {:ok, string_part, rest_part} = from_binary(bin)
    {string_part, rest_part}
  end

  @doc """
  Creates a Lighttel string from a UTF-8 encoded string

  ##Examples

      iex>Lighttel.String.to_binary("123")
      {:ok, <<0, 3, ?1, ?2, ?3>>}

      iex>Lighttel.String.to_binary(<<0,1,129>>)
      {:error, :invalid_string}

  """
  def to_binary(<<string::binary>>) do
    use Bitwise

    if String.valid?(string) do
      msb_len = byte_size(string) >>> 16 &&& 0x00FF
      lsb_len = byte_size(string) &&& 0x00FF
      {:ok, <<msb_len, lsb_len>> <> string}
    else
      {:error, :invalid_string}
    end
  end

  def to_binary(string) when is_list(string) do
    to_binary(List.to_string(string))
  end

  def to_binary!(string) when is_binary(string) do
    {:ok, data} = to_binary(string)
    data
  end
end
