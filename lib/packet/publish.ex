defmodule Lighttel.Packet.Publish do
  @behaviour Lighttel.Packet

  defstruct message_id: 0,
            endpoint: "",
            headers: %{},
            payload: <<>>

  alias __MODULE__

  def type do
    23
  end

  @doc """
  Builds a publish structure from binary payload

  ## Examples

    iex>Lighttel.Packet.Publish.from_payload(<<257 :: big-16,0, 514 :: big-16,2+5+2+5,0,5,"hello", 0,5,"world",1,2,3>>, [])
    %Lighttel.Packet.Publish{
        message_id: 257,
        endpoint: (256*2+2),
        headers: %{"hello"=> ["world"]},
        payload: <<1,2,3>>
    }

  """
  def from_payload(
        <<message_id::big-16, endpoint_type::size(1), 0::size(6), compression::size(1),
          rest::binary>>,
        opts
      ) do
    {ep, rest} =
      if endpoint_type === 1 do
        <<sz::big-16, ep::binary-size(sz), rest::binary>> = rest
        {ep, rest}
      else
        <<ep::big-16, rest::binary>> = rest
        {ep, rest}
      end

    compression = Lighttel.Compression.bitfield_to_single_algo(compression)
    decompress_max_size = Keyword.get(opts, :max_size)

    {:ok, m_h_len, data_rest} = Lighttel.Packet.FixedHeader.decode_remaining_length(rest)

    decompressed_data =
      Lighttel.Compression.decompress(data_rest, compression, decompress_max_size)

    <<message_header_data::binary-size(m_h_len), payload::binary>> = decompressed_data

    header_values = headers_from_binary!(message_header_data)

    %Publish{
      message_id: message_id,
      endpoint: ep,
      headers: header_values,
      payload: payload
    }
  end

  @doc """
  Converts a packet structure to its binary representation

  ## Examples

    iex>Lighttel.Packet.Publish.payload(%Lighttel.Packet.Publish{message_id: 257,
    ...>endpoint: (256*2+2),
    ...>payload: <<1,2,3>>
    ...>}, [])
    <<257 :: big-16,0, 514 :: big-16,0,1,2,3>>

  """

  def payload(
        %Lighttel.Packet.Publish{
          message_id: message_id,
          endpoint: ep,
          payload: payload,
          headers: headers
        },
        opts
      ) do
    {ep_type, ep_data} =
      if is_integer(ep) do
        {0, <<ep::big-16>>}
      else
        {1, Lighttel.String.to_binary!(ep)}
      end

    compression = Keyword.get(opts, :compression, :uncompressed)
    compression_level = Keyword.get(opts, :compression_level, 9)

    headers_bin_data = headers_to_binary!(headers)

    {:ok, rle_header_len} =
      Lighttel.Packet.FixedHeader.encode_remaining_length(byte_size(headers_bin_data))

    {algo, compressed_data} =
      Lighttel.Compression.compress(headers_bin_data <> payload, compression, compression_level)

    actual_used = Lighttel.Compression.single_algo_to_bitfield(algo)

    <<message_id::big-16, ep_type::size(1), 0::size(6), actual_used::size(1)>> <>
      ep_data <> rle_header_len <> compressed_data
  end

  def headers_from_binary!(<<data::binary>>) do
    header_values = parse_header_values!(data, [])

    header_values
    |> Enum.reduce(%{}, fn {key, val}, acc ->
      new_val = Map.get(acc, key, []) ++ [val]
      Map.put(acc, key, new_val)
    end)
  end

  defp parse_header_values!(<<>>, acc) do
    acc
  end

  defp parse_header_values!(
         <<key_len::big-16, key::binary-size(key_len), val_len::big-16, val::binary-size(val_len),
           rest::binary>>,
         acc
       ) do
    parse_header_values!(rest, acc ++ [{key, val}])
  end

  @doc """
  Encodes a header field to its binary representation

  """
  def headers_to_binary!(headers) when is_map(headers) do
    headers
    |> Enum.reduce(<<>>, fn {key, val}, acc ->
      key_val_bin =
        val
        |> Enum.reduce(<<>>, fn single_val, acc ->
          acc <> Lighttel.String.to_binary!(key) <> Lighttel.String.to_binary!(single_val)
        end)

      acc <> key_val_bin
    end)
  end

  def headers_data_size(headers) when is_map(headers) do
    headers
    |> Enum.reduce(0, fn {key, val}, acc ->
      size =
        val
        |> Enum.reduce(0, fn single_val, acc ->
          acc + 4 + byte_size(key) + byte_size(single_val)
        end)

      acc + size
    end)
  end

  @doc """
  Adds header values to a publish message

  ## Examples

      iex>Lighttel.Packet.Publish.add_header(%Lighttel.Packet.Publish{}, {"hello", ["world", "data"]})
      %Lighttel.Packet.Publish{
        headers: %{"hello" => ["world", "data"]}
      }

      Lighttel.Packet.Publish.add_header(%Lighttel.Packet.Publish{}, [{"hello", ["world", "data"]}, {"abc", "def"}])
      %Lighttel.Packet.Publish{
        headers: %{"hello" => ["world", "data"],
        "abc" => ["def"]}
      }

  """
  @spec add_header(map, list | {binary, binary} | {binary, list}) :: map
  def add_header(%Publish{headers: headers} = message, {key, values})
      when is_binary(key) and is_list(values) do
    new_headers =
      headers
      |> Map.update(key, values, fn old_val ->
        old_val ++ values
      end)

    %Publish{message | headers: new_headers}
  end

  def add_header(message, {key, value}) when is_binary(key) and is_binary(value) do
    add_header(message, {key, [value]})
  end

  def add_header(message, [{_key, _value} = val | rest]) do
    add_header(message, val)
    |> add_header(rest)
  end

  def add_header(message, []) do
    message
  end

  @doc """
  Delete a header item.

  If a value is supplied only that value is deleted, otherwise the entire key is removed.
  If a key has no values after a deletion the entire key is removed.

  ## Examples

      iex>Lighttel.Packet.Publish.delete_header(%Lighttel.Packet.Publish{
      ...>headers: %{"hello" => ["world", "baz"], "baz" => ["foo"]}
      ...>}, "hello")
      %Lighttel.Packet.Publish{
        headers: %{"baz" => ["foo"]}
      }

      iex>Lighttel.Packet.Publish.delete_header(%Lighttel.Packet.Publish{
      ...>headers: %{"hello" => ["world", "baz"], "baz" => ["foo"]}
      ...>}, "hello", "world")
      %Lighttel.Packet.Publish{
        headers: %{"hello" => ["baz"],"baz" => ["foo"]}
      }

      iex>Lighttel.Packet.Publish.delete_header(%Lighttel.Packet.Publish{
      ...>headers: %{"hello" => ["world", "baz"], "baz" => ["foo"]}
      ...>}, "baz", "foo")
      %Lighttel.Packet.Publish{
        headers: %{"hello" => ["world", "baz"]}
      }

  """
  @spec delete_header(map, binary, binary | nil) :: map
  def delete_header(message, key, value \\ nil)

  def delete_header(%Publish{headers: headers} = message, key, nil) do
    %Publish{message | headers: Map.delete(headers, key)}
  end

  def delete_header(%Publish{headers: headers} = message, key, value) when is_binary(value) do
    stored_values = Map.get(headers, key)

    new_values =
      if stored_values !== nil do
        new_values =
          Enum.reduce(stored_values, [], fn x, acc ->
            if x === value, do: acc, else: acc ++ [x]
          end)

        if List.first(new_values) === nil do
          nil
        else
          new_values
        end
      end

    new_headers =
      if new_values === nil do
        Map.delete(headers, key)
      else
        Map.put(headers, key, new_values)
      end

    %Publish{message | headers: new_headers}
  end
end
