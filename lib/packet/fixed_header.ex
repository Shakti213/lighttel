defmodule Lighttel.Packet.FixedHeader do
  alias __MODULE__

  @moduledoc """
  Contains the definition of fixed header
  as well as serializing to and from binary representations.
  """
  defstruct type: 0, remaining_length: 0

  @doc """
  Converts binary data to a FixedHeader and returns the remaining data
  Returns either {:ok, `length`, `rest_of_data`}
  or {:error, error_info}

  ## Example

      iex>Lighttel.Packet.FixedHeader.from_binary(<<>>)
      {:error, :need_more_data, <<>>}

      iex>Lighttel.Packet.FixedHeader.from_binary(<<1, 128, 128, 128, 128, 128>>)
      {:error, :bad_packet}

      iex>Lighttel.Packet.FixedHeader.from_binary(<<3, 64, 123, 54>>)
      {:ok, %Lighttel.Packet.FixedHeader{type: 3, remaining_length: 64}, <<123, 54>>}

      iex>Lighttel.Packet.FixedHeader.from_binary(<<3, 0xff, 0x80, 0x80, 0x00, 121, 53>>)
      {:ok, %Lighttel.Packet.FixedHeader{type: 3, remaining_length: 266338304}, <<121, 53>>}

  """
  @spec from_binary(binary) :: {:ok, %FixedHeader{}, binary} | {:error, atom}
  def from_binary(<<type, rest::binary>>) when byte_size(rest) > 0 do
    case decode_remaining_length(rest) do
      {:ok, length, rest} ->
        {:ok, %FixedHeader{type: type, remaining_length: length}, rest}

      error ->
        error
    end
  end

  def from_binary(<<b::binary>>) when byte_size(b) <= 1 do
    {:error, :need_more_data, b}
  end

  def from_binary!(bin) do
    {:ok, header, rest} = from_binary(bin)
    {header, rest}
  end

  @doc """
  Converts a fixed header to binary data

  ## Examples

      iex>Lighttel.Packet.FixedHeader.to_binary(%Lighttel.Packet.FixedHeader{type: 2, remaining_length: 128})
      {:ok, <<2, 129, 0>>}

  """
  @spec to_binary(%FixedHeader{}) :: {:ok, binary} | {:error, atom}
  def to_binary(%FixedHeader{type: t, remaining_length: len}) when t < 256 do
    case length_to_binary(len) do
      {:ok, bin} -> {:ok, <<t>> <> bin}
      error -> error
    end
  end

  @spec to_binary(%FixedHeader{}) :: binary
  def to_binary!(header) do
    {:ok, bin} = to_binary(header)
    bin
  end

  @doc """
  Decodes the remaining length

  ## Examples

    iex>Lighttel.Packet.FixedHeader.decode_remaining_length(<<0x81, 0x00>>)
    {:ok, 128, <<>>}

  """
  @spec decode_remaining_length(binary) :: {:ok, integer, binary} | {:error, atom}
  def decode_remaining_length(data) when is_binary(data) do
    data_len = min(4, byte_size(data))
    length_candidates = :binary.bin_to_list(data, 0, data_len)
    last_len_index = Enum.find_index(length_candidates, fn x -> x < 128 end)

    if last_len_index == nil do
      if data_len == 4, do: {:error, :bad_packet}, else: {:error, :need_more_data, data}
    else
      length_reducer = fn e, acc ->
        use Bitwise
        {halt_cont, this_val} = if e >= 128, do: {:cont, e &&& 0x07F}, else: {:halt, e}
        new_acc = (acc <<< 7) + this_val
        {halt_cont, new_acc}
      end

      rem_len =
        length_candidates
        |> Enum.reduce_while(0, length_reducer)

      {:ok, rem_len,
       :binary.part(data, last_len_index + 1, byte_size(data) - (1 + last_len_index))}
    end
  end

  @doc """
  Encodes an integer with the remaining length scheme

  ## Examples

    iex>Lighttel.Packet.FixedHeader.encode_remaining_length(128)
    {:ok, <<0x81,0x00>>}

  """
  @spec encode_remaining_length(integer) :: binary
  def encode_remaining_length(len) when is_integer(len) do
    length_to_binary(len)
  end

  defp length_to_binary(len) when len <= 0xFFFFFFF do
    bin =
      len
      |> length_to_list()
      |> :binary.list_to_bin()

    {:ok, bin}
  end

  defp length_to_binary(l) when is_integer(l) do
    {:error, :unsupported_length}
  end

  defp length_to_list(length) do
    ordered_list =
      []
      |> length_to_list(length)
      |> Enum.reverse()

    list_len = length(ordered_list)

    {prefixed, _} =
      ordered_list
      |> Enum.map_reduce(0, fn x, acc ->
        ret_x = if acc == list_len - 1, do: x, else: 0x80 + x
        {ret_x, acc + 1}
      end)

    prefixed
  end

  defp length_to_list(list, length) when length < 128 do
    list ++ [length]
  end

  defp length_to_list(list, length) do
    use Bitwise
    new_list = list ++ [length &&& 0x07F]
    length_to_list(new_list, length >>> 7)
  end
end
