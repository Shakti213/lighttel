defmodule Lighttel.Packet.BatchPublish do
  @behaviour Lighttel.Packet

  @moduledoc """
  Defines the Batch publish packet as specified in the lighttel specification
  """

  alias __MODULE__

  defstruct messages: []

  def type do
    24
  end

  def from_payload(<<compression, data::binary>>, opts) do
    messages =
      impl_from_payload(data, Lighttel.Compression.bitfield_to_single_algo(compression), opts)

    %BatchPublish{
      messages: messages
    }
  end

  defp impl_from_payload(<<>>, :uncompressed, _opts) do
    []
  end

  defp impl_from_payload(data, :uncompressed, opts) do
    {:ok, var_len, rest_of_data} = Lighttel.Packet.FixedHeader.decode_remaining_length(data)
    <<message_id::big-16, ep_type, rest_of_data::binary>> = rest_of_data

    {ep, var_data, next_packet} =
      if ep_type === 0 do
        <<ep::big-16, var_data::binary-size(var_len), rest::binary>> = rest_of_data
        {ep, var_data, rest}
      else
        <<ep_len::big-16, ep::binary-size(ep_len), var_data::binary-size(var_len), rest::binary>> =
          rest_of_data

        {ep, var_data, rest}
      end

    {:ok, headers_len, headers_and_payload} =
      Lighttel.Packet.FixedHeader.decode_remaining_length(var_data)

    <<header_data::binary-size(headers_len), payload::binary>> = headers_and_payload

    [
      %Lighttel.Packet.Publish{
        message_id: message_id,
        endpoint: ep,
        payload: payload,
        headers: Lighttel.Packet.Publish.headers_from_binary!(header_data)
      }
    ] ++ impl_from_payload(next_packet, :uncompressed, opts)
  end

  defp impl_from_payload(data, compression, opts) do
    max_size = Keyword.get(opts, :max_size)

    data
    |> Lighttel.Compression.decompress(compression, max_size)
    |> impl_from_payload(:uncompressed, opts)
  end

  def payload(
        %BatchPublish{
          messages: messages
        },
        opts
      ) do
    compression = Keyword.get(opts, :compression, :uncompressed)
    compression_level = Keyword.get(opts, :compression_level, 9)

    payload =
      messages
      |> Enum.reduce(<<>>, fn m, a ->
        payload = Lighttel.Packet.Publish.payload(m, compression: :uncompressed)
        headers_data_size = Lighttel.Packet.Publish.headers_data_size(m.headers)

        {:ok, headers_data_len} =
          Lighttel.Packet.FixedHeader.encode_remaining_length(headers_data_size)

        header_payload_len =
          byte_size(m.payload) + headers_data_size + byte_size(headers_data_len)

        {:ok, len} = Lighttel.Packet.FixedHeader.encode_remaining_length(header_payload_len)
        a <> len <> payload
      end)

    {algo, payload} = Lighttel.Compression.compress(payload, compression, compression_level)
    compression = Lighttel.Compression.single_algo_to_bitfield(algo)
    <<compression>> <> payload
  end
end
