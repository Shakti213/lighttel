defmodule Lighttel.Packet.Subscription do
  @behaviour Lighttel.Packet

  alias __MODULE__

  defstruct endpoints: %{}

  @impl true
  def type() do
    30
  end

  @doc """
  Builds a Subscription structure from a binary payload

  ## Examples

    iex>Lighttel.Packet.Subscription.from_payload(<< 256 :: big-16, 6 :: big-16, "$hello" >>, [])
    %Lighttel.Packet.Subscription { endpoints: %{"$hello" => 256}}

    iex>Lighttel.Packet.Subscription.from_payload(<< 256 :: big-16, 6 :: big-16, "$hello" >> <> << 256 :: big-16, 6 :: big-16, "$world" >>, [])
    %Lighttel.Packet.Subscription { endpoints: %{"$hello" => 0, "$world" => 0}}

    iex>Lighttel.Packet.Subscription.from_payload(<< 256 :: big-16, 6 :: big-16, "$hello" >> <> << 257 :: big-16, 6 :: big-16, "$world" >>, [])
    %Lighttel.Packet.Subscription { endpoints: %{"$hello" => 256, "$world" => 257}}

  """
  @impl true
  def from_payload(<<data::binary>>, _opts) do
    eps = parse_payload(data, %{})

    eps = uniquify_endpoints(eps)

    %Subscription{endpoints: eps}
  end

  defp parse_payload(<<>>, acc) do
    acc
  end

  defp parse_payload(
         <<handle_id::big-16, ep_size::big-16, ep_name::binary-size(ep_size), rest::binary>>,
         acc
       ) do
    parse_payload(rest, Map.put(acc, ep_name, handle_id))
  end

  @doc """
  Creates a binary payload from a Subscription structure

  ## Examples

    iex>Lighttel.Packet.Subscription.payload(%Lighttel.Packet.Subscription{endpoints: %{"$hello" => 256}}, [])
    << 256 :: big-16, 6 :: big-16, "$hello" >>

    iex>Lighttel.Packet.Subscription.payload(%Lighttel.Packet.Subscription{endpoints: %{"$hello" => 256, "$world" => 256}}, [])
    << 0 :: big-16, 6 :: big-16, "$hello", 0 :: big-16, 6 :: big-16, "$world" >>

  """
  @impl true
  def payload(%Subscription{endpoints: eps}, _opts) do
    for {ep_name, ep_handle} <- uniquify_endpoints(eps),
        into: <<>>,
        do: <<ep_handle::big-16>> <> Lighttel.String.to_binary!(ep_name)
  end

  @doc """
  Adds a new endpoint to the subscription. If the handle is already taken it will be assigned handle 0 instead

  ## Examples

    iex>Lighttel.Packet.Subscription.put(%Lighttel.Packet.Subscription{}, "@hello", 1)
    {1, %Lighttel.Packet.Subscription{ endpoints: %{"@hello" => 1}}}
    
    iex>Lighttel.Packet.Subscription.put(%Lighttel.Packet.Subscription{endpoints: %{"@world" => 1}}, "@hello", 1)
    {0, %Lighttel.Packet.Subscription{ endpoints: %{"@hello" => 0, "@world" => 1}}}

  """
  def put(%Subscription{endpoints: eps}, ep, handle) do
    if Enum.find_index(eps, fn {_e, h} -> h === handle end) === nil do
      {handle, %Subscription{endpoints: Map.put(eps, ep, handle)}}
    else
      {0, %Subscription{endpoints: Map.put(eps, ep, 0)}}
    end
  end

  def get(%Subscription{endpoints: eps}, handle) when is_integer(handle) do
    case Enum.find(eps, nil, fn {_ep, h} -> h === handle end) do
      nil -> :error
      {ep, _h} -> {:ok, ep}
    end
  end

  def get(%Subscription{endpoints: eps}, ep) when is_binary(ep) do
    eps[ep]
  end

  def merge(%Subscription{endpoints: left}, %Subscription{endpoints: right}) do
    eps =
      Map.merge(left, right, fn _, v1, v2 -> if v1 === v2, do: v1, else: 0 end)
      |> uniquify_endpoints()

    %Subscription{endpoints: eps}
  end

  @doc """
  Calculates and returns the next available handle given a subscription

  ## Examples

    iex>Lighttel.Packet.Subscription.next_handle(%Lighttel.Packet.Subscription{})
    1

    iex>Lighttel.Packet.Subscription.next_handle(%Lighttel.Packet.Subscription{ endpoints: %{"ab" => 1}})
    2

    iex>Lighttel.Packet.Subscription.next_handle(%Lighttel.Packet.Subscription{ endpoints: %{"ab" => 1, "cd" => 3}})
    2

    iex>Lighttel.Packet.Subscription.next_handle(%Lighttel.Packet.Subscription{ endpoints: %{"ab" => 1, "cd" => 2}})
    3

  """
  def next_handle(%Subscription{endpoints: eps}) do
    if Enum.count(eps) === 0 do
      1
    else
      next_handle =
        eps
        |> Enum.sort_by(fn {_e, h} -> h end)
        |> Enum.reduce_while(1, fn {_e, h}, acc ->
          if h === acc, do: {:cont, acc + 1}, else: {:halt, acc}
        end)

      if next_handle <= 65535, do: next_handle, else: 0
    end
  end

  defp uniquify_endpoints(endpoints) do
    endpoints
    # Build a map on the form %{handle1 => [ep1, ep2...], handle2 => [ep3, ep4...]}
    |> Enum.reduce(%{}, fn {ep, h}, acc -> Map.put(acc, h, Map.get(acc, h, []) ++ [ep]) end)
    # Create a map each handle that contains more than 1 endpoint gets the handle 0 assigned instead
    # otherwise simply reverse the mapping back to %{ep1 => handl1, ep2 => handle2...}
    |> Enum.reduce(%{}, fn {h, ep}, acc ->
      ep_len = length(ep)

      if h === 0 or ep_len !== 1 do
        # Insert all endpoints with 0 handle
        Map.merge(acc, Map.new(ep, fn e -> {e, 0} end))
      else
        [ep] = ep
        Map.put(acc, ep, h)
      end
    end)
  end
end
