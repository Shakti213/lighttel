defmodule Lighttel.Packet.PutAuth.Nack do
  @behaviour Lighttel.Packet

  alias __MODULE__

  defstruct handle: 0,
            reason: 0

  def type do
    12
  end

  def from_payload(<<handle::big-16, reason>>, _opts) do
    %Nack{
      handle: handle,
      reason: reason
    }
  end

  def payload(
        %Nack{
          handle: handle,
          reason: reason
        },
        _opts
      ) do
    <<handle::big-16, reason>>
  end
end
